package com.magdy.trainingcenter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magdy.trainingcenter.entity.Course;
import com.magdy.trainingcenter.service.CourseService;

@RestController
@RequestMapping("/v1/courses")
public class CourseController {
	@Autowired
	private CourseService service;

	@GetMapping
	public List<Course> getAll(@RequestParam(required = false, defaultValue = "0") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "10") Integer size,
			@RequestParam(required = false, defaultValue = "name") String sortBy,
			@RequestParam(required = false, defaultValue = "ASC") String direction,
			@RequestParam(required = false, defaultValue = "") String name,
			@RequestParam(required = false, defaultValue = "") String category) {
		Pageable page = PageRequest.of(pageNumber, size, Sort.by(Direction.fromString(direction), sortBy));
		return service.getAll(name, category, page);
	}

	@GetMapping("/{id}")
	public Course getById(@PathVariable Long id) {
		return service.getById(id);
	}
}

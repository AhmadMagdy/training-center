package com.magdy.trainingcenter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magdy.trainingcenter.dto.TraineeDto;
import com.magdy.trainingcenter.entity.Enrollment;
import com.magdy.trainingcenter.entity.Trainee;
import com.magdy.trainingcenter.service.TraineeService;

@RestController
@RequestMapping("/v1/trainees")
public class TraineeController {
	@Autowired
	private TraineeService service;

	@GetMapping
	public List<Trainee> getAll(@RequestParam(required = false, defaultValue = "0") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "10") Integer size,
			@RequestParam(required = false, defaultValue = "name") String sortBy,
			@RequestParam(required = false, defaultValue = "ASC") String direction,
			@RequestParam(required = false, defaultValue = "") String name,
			@RequestParam(required = false, defaultValue = "") String email,
			@RequestParam(required = false) Long courseId) {
		Pageable page = PageRequest.of(pageNumber, size, Sort.by(Direction.fromString(direction), sortBy));
		return service.getAll(page, name, email, courseId);
	}

	@PostMapping
	public Trainee add(@RequestBody TraineeDto trainer) {
		return service.add(trainer);
	}

	@GetMapping("/{id}")
	public Trainee getById(@PathVariable Long id) {
		return service.getById(id);
	}

	@PostMapping("/{id}/courses/{courseId}")
	public Enrollment enrollCourse(@PathVariable Long id, @PathVariable Long courseId) {
		return service.enrollCourse(id, courseId);
	}
}

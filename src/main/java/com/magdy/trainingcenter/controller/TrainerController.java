package com.magdy.trainingcenter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magdy.trainingcenter.dto.CourseDto;
import com.magdy.trainingcenter.dto.TrainerDto;
import com.magdy.trainingcenter.entity.Course;
import com.magdy.trainingcenter.entity.Trainer;
import com.magdy.trainingcenter.service.TrainerService;

@RestController
@RequestMapping("/v1/trainers")
public class TrainerController {

	@Autowired
	private TrainerService service;

	@GetMapping
	public List<Trainer> getAll(@RequestParam(required = false, defaultValue = "0") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "10") Integer size,
			@RequestParam(required = false, defaultValue = "name") String sortBy,
			@RequestParam(required = false, defaultValue = "ASC") String direction,
			@RequestParam(required = false, defaultValue = "") String name,
			@RequestParam(required = false, defaultValue = "") String email) {
		Pageable page = PageRequest.of(pageNumber, size, Sort.Direction.fromString(direction), sortBy);
		return service.getAll(page, name, email);
	}

	@GetMapping("/{id}")
	public Trainer getById(@PathVariable Long id) {
		return service.getById(id);
	}

	@PostMapping
	public Trainer add(@RequestBody TrainerDto dto) {
		return service.add(dto);
	}

	@PutMapping("/{id}")
	public Trainer update(@PathVariable Long id, @RequestBody TrainerDto dto) {
		return service.update(id, dto);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		service.delete(id);
	}

	@GetMapping("/{id}/courses")
	public List<Course> getTrainerCourses(@PathVariable Long id,
			@RequestParam(required = false, defaultValue = "0") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "10") Integer size,
			@RequestParam(required = false, defaultValue = "name") String sortBy,
			@RequestParam(required = false, defaultValue = "ASC") String direction) {
		Pageable page = PageRequest.of(pageNumber, size, Sort.by(Direction.fromString(direction), sortBy));
		return service.getTrainerCourses(id, page);
	}

	@PostMapping("/{id}/courses")
	public Course add(@PathVariable Long id, @RequestBody CourseDto course) {
		return service.addCourse(id, course);
	}

	@PutMapping("/{id}/courses/{courseId}")
	public Course updateCourse(@PathVariable Long id, @PathVariable Long courseId, @RequestBody CourseDto course) {
		return service.updateCourse(id, courseId, course);
	}

	@DeleteMapping("/{id}/courses/{courseId}")
	public void deleteCourse(@PathVariable Long id, @PathVariable Long courseId) {
		service.deleteCourse(id, courseId);
	}
}

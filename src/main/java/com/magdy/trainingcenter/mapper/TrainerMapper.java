package com.magdy.trainingcenter.mapper;

import org.springframework.stereotype.Component;

import com.magdy.trainingcenter.dto.TrainerDto;
import com.magdy.trainingcenter.entity.Trainer;

@Component
public class TrainerMapper implements Mapper<TrainerDto, Trainer> {

	@Override
	public Trainer toEntity(TrainerDto dto) {
		Trainer entity = new Trainer(null, dto.getName().trim(), dto.getHourlyRate(), dto.getEmail().trim());
		return entity;
	}

	@Override
	public TrainerDto toDto(Trainer entity) {
		TrainerDto dto = new TrainerDto(entity.getId(), entity.getName(), entity.getHourlyRate(), entity.getEmail());
		return dto;
	}

	@Override
	public Trainer updateEntity(Trainer entity, TrainerDto dto) {
		entity.setName(dto.getName().trim());
		entity.setHourlyRate(dto.getHourlyRate());
		return entity;
	}

}

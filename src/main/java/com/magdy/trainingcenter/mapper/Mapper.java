package com.magdy.trainingcenter.mapper;

public interface Mapper<D, E> {
	public E toEntity(D dto);

	public D toDto(E entity);

	public E updateEntity(E entity, D dto);
}

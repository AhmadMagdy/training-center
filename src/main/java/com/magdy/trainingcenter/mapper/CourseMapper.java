package com.magdy.trainingcenter.mapper;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.magdy.trainingcenter.dto.CourseDto;
import com.magdy.trainingcenter.entity.Course;

@Component
public class CourseMapper implements Mapper<CourseDto, Course> {

	@Override
	public Course toEntity(CourseDto dto) {
		Course entity = new Course(null, dto.getHours(), dto.getName().trim(), dto.getCategory().trim(), dto.getPrice(),
				new Date(), dto.getTrainer());
		return entity;
	}

	@Override
	public CourseDto toDto(Course entity) {
		CourseDto dto = new CourseDto(entity.getId(), entity.getHours(), entity.getName(), entity.getCategory(),
				entity.getPrice(), entity.getRegisteredAt(), entity.getTrainer());
		return dto;
	}

	@Override
	public Course updateEntity(Course entity, CourseDto dto) {
		entity.setName(dto.getName().trim());
		entity.setHours(dto.getHours());
		entity.setPrice(dto.getPrice());
		entity.setCategory(dto.getCategory());
		return entity;
	}

}

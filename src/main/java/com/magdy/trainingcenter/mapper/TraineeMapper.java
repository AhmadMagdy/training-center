package com.magdy.trainingcenter.mapper;

import org.springframework.stereotype.Component;

import com.magdy.trainingcenter.dto.TraineeDto;
import com.magdy.trainingcenter.entity.Trainee;

@Component
public class TraineeMapper implements Mapper<TraineeDto, Trainee> {

	@Override
	public Trainee toEntity(TraineeDto dto) {
		Trainee entity = new Trainee(null, dto.getName().trim(), dto.getEmail().trim());
		return entity;
	}

	@Override
	public TraineeDto toDto(Trainee entity) {
		TraineeDto dto = new TraineeDto(entity.getId(), entity.getName(), entity.getEmail());
		return dto;
	}

	@Override
	public Trainee updateEntity(Trainee entity, TraineeDto dto) {
		entity.setName(dto.getName().trim());
		return entity;
	}

}

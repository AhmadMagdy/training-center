package com.magdy.trainingcenter.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.magdy.trainingcenter.entity.Trainer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CourseDto {
	@JsonProperty(access = Access.READ_ONLY)
	private Long id;
	private Integer hours;
	private String name;
	private String category;
	private Double price;
	@JsonProperty(access = Access.READ_ONLY)
	private Date registeredAt;
	@JsonProperty(access = Access.READ_ONLY)
	private Trainer trainer;
}

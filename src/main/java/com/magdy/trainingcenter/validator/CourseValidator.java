package com.magdy.trainingcenter.validator;

import com.magdy.trainingcenter.dto.CourseDto;

public class CourseValidator extends Validator<CourseDto> {

	@Override
	public void validateOnInsert(CourseDto dto) {
		validateString(dto.getName(), "course.name.invalid");
		validateString(dto.getCategory(), "course.category.invalid");
		validateNumber(dto.getHours(), 1D, 200D, "course.hours.invalid");
		validateNumber(dto.getPrice(), 0D, null, "course.price.invalid");
	}

	@Override
	public void validateOnUpdate(CourseDto dto) {
		validateOnInsert(dto);
	}
}

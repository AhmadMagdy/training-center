package com.magdy.trainingcenter.validator;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.http.HttpStatus;

import com.magdy.trainingcenter.exception.CustomException;

public abstract class Validator<T> {
	private static final int STRING_MIN_LENGTH = 3;
	private static final int STRING_MAX_LENGTH = 64;
	private static final EmailValidator EMAIL_VALIDATOR = EmailValidator.getInstance();

	abstract void validateOnInsert(T value);

	abstract void validateOnUpdate(T value);

	public void validateString(String s, String code) {
		if (s == null || s.isEmpty() || s.trim().isEmpty())
			throw new CustomException(code, HttpStatus.NOT_ACCEPTABLE);
		int size = s.length();
		if (size < STRING_MIN_LENGTH || size > STRING_MAX_LENGTH)
			throw new CustomException(code, HttpStatus.NOT_ACCEPTABLE);
	}

	public void validateNumber(Object n, Double min, Double max, String code) {
		if (n == null)
			throw new CustomException(code, HttpStatus.NOT_ACCEPTABLE);
		Double number = Double.valueOf(n.toString());
		if (max != null && number > max)
			throw new CustomException(code, HttpStatus.NOT_ACCEPTABLE);
		if (min != null && number < min)
			throw new CustomException(code, HttpStatus.NOT_ACCEPTABLE);
	}

	public void validateUrl(String fileUrl) {
		if (!UrlValidator.getInstance().isValid(fileUrl))
			throw new CustomException("url.invalid", HttpStatus.NOT_ACCEPTABLE);
	}

	public void validateEmail(String email) {
		if (email == null)
			throw new CustomException("email.missing", HttpStatus.NOT_ACCEPTABLE);
		if (!EMAIL_VALIDATOR.isValid(email))
			throw new CustomException("email.invalid", HttpStatus.NOT_ACCEPTABLE);
	}
}

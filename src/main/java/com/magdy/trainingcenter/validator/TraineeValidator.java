package com.magdy.trainingcenter.validator;

import com.magdy.trainingcenter.dto.TraineeDto;

public class TraineeValidator extends Validator<TraineeDto> {
	@Override
	public void validateOnInsert(TraineeDto value) {
		validateString(value.getName(), "trainee.name.invalid");
		validateEmail(value.getEmail());
	}

	@Override
	public void validateOnUpdate(TraineeDto value) {
		validateString(value.getName(), "trainee.name.invalid");
	}
}

package com.magdy.trainingcenter.validator;

import com.magdy.trainingcenter.dto.TrainerDto;

public class TrainerValidator extends Validator<TrainerDto> {
	@Override
	public void validateOnInsert(TrainerDto value) {
		validateString(value.getName(), "trainer.name.invalid");
		validateNumber(value.getHourlyRate(), 0D, 100D, "trainer.hour_salary.invalid");
		validateEmail(value.getEmail());
	}

	@Override
	public void validateOnUpdate(TrainerDto value) {
		validateString(value.getName(), "trainer.name.invalid");
		validateNumber(value.getHourlyRate(), 0D, 100D, "trainer.hour_salary.invalid");
	}
}

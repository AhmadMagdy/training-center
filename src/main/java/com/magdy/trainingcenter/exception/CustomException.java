package com.magdy.trainingcenter.exception;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
public class CustomException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	String code;
	HttpStatus status;

	public CustomException(String code, HttpStatus status) {
		this.code = code;
		this.status = status;
	}
}

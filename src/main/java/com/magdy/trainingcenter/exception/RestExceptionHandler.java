package com.magdy.trainingcenter.exception;

import java.util.Locale;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {
	private final MessageSource messageSource;

	@Autowired
	public RestExceptionHandler(@Qualifier("message_resource") MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@ExceptionHandler(CustomException.class)
	public ResponseEntity<?> handleCustomException(CustomException ex, Locale locale) {
		ErrorMessage errorMessage = new ErrorMessage();
		String message = messageSource.getMessage(ex.getCode(), new Object[] {}, locale);
		errorMessage.setMessage(message);
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), ex.getStatus());
	}

	@ExceptionHandler(ValidationException.class)
	public ResponseEntity<?> handleValidationException(ValidationException ex, Locale locale) {
		if (ex.getCause() instanceof CustomException)
			return handleCustomException((CustomException) ex.getCause(), locale);
		ex.printStackTrace();
		return new ResponseEntity<>(new ErrorMessage("Data is not valid"), new HttpHeaders(),
				HttpStatus.NOT_ACCEPTABLE);
	}
}

package com.magdy.trainingcenter.exception;

import java.util.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
	public ErrorMessage(String message) {
		this.message = message;
	}

	String message;
	String id = UUID.randomUUID().toString();
	Date time = new Date();
}

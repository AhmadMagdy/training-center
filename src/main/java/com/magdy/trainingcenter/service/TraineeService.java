package com.magdy.trainingcenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.magdy.trainingcenter.dto.TraineeDto;
import com.magdy.trainingcenter.entity.Enrollment;
import com.magdy.trainingcenter.entity.Trainee;
import com.magdy.trainingcenter.exception.CustomException;
import com.magdy.trainingcenter.mapper.TraineeMapper;
import com.magdy.trainingcenter.repository.TraineeRepository;
import com.magdy.trainingcenter.validator.TraineeValidator;

@Service
public class TraineeService {
	@Autowired
	private TraineeRepository repository;
	@Autowired
	private CourseService courseService;
	@Autowired
	private TraineeMapper mapper;
	private final static TraineeValidator VALIDATOR = new TraineeValidator();

	public Trainee add(TraineeDto dto) {
		VALIDATOR.validateOnInsert(dto);
		Trainee trainee = mapper.toEntity(dto);
		return repository.save(trainee);
	}

	public Trainee getById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new CustomException("trainee.not_found", HttpStatus.NOT_FOUND));
	}

	public Enrollment enrollCourse(Long traineeId, Long courseId) {
		Trainee trainee = getById(traineeId);
		return courseService.enrollCourse(courseId, trainee);
	}

	public List<Trainee> getAll(Pageable page, String name, String email, Long courseId) {
		if (courseId != null)
			return repository.filter(name, email, courseId, page);
		return repository.filter(name, email, page);
	}
}

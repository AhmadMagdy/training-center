package com.magdy.trainingcenter.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.magdy.trainingcenter.dto.CourseDto;
import com.magdy.trainingcenter.entity.Course;
import com.magdy.trainingcenter.entity.Enrollment;
import com.magdy.trainingcenter.entity.Trainee;
import com.magdy.trainingcenter.entity.Trainer;
import com.magdy.trainingcenter.exception.CustomException;
import com.magdy.trainingcenter.mapper.CourseMapper;
import com.magdy.trainingcenter.repository.CourseRepository;
import com.magdy.trainingcenter.repository.EnrollmentRepository;
import com.magdy.trainingcenter.validator.CourseValidator;

@Service
public class CourseService {
	@Autowired
	private CourseRepository repository;
	@Autowired
	private EnrollmentRepository enrollmentRepository;
	@Autowired
	private CourseMapper mapper;
	private final static CourseValidator VALIDATOR = new CourseValidator();

	public Course add(CourseDto dto) {
		VALIDATOR.validateOnInsert(dto);
		Course course = mapper.toEntity(dto);
		if (checkIfExists(null, course.getName(), course.getCategory(), course.getTrainer()))
			throw new CustomException("course.duplicated", HttpStatus.CONFLICT);
		return repository.save(course);
	}

	public boolean checkIfExists(Long id, String name, String category, Trainer trainer) {
		if (id != null) {
			Optional<Course> course = repository.findByIdNotAndNameIgnoreCaseAndCategoryIgnoreCaseAndTrainer(id, name,
					category, trainer);
			return course.isPresent();
		}
		Optional<Course> course = repository.findByNameIgnoreCaseAndCategoryIgnoreCaseAndTrainer(name, category,
				trainer);
		return course.isPresent();
	}

	public Course getById(Long id) {
		return repository.findById(id).orElseThrow(() -> new CustomException("course.not_found", HttpStatus.NOT_FOUND));
	}

	public Enrollment enrollCourse(Long courseId, Trainee trainee) {
		Course course = getById(courseId);
		if (checkIfEnrollmentExists(course, trainee))
			throw new CustomException("enrollment.duplicated", HttpStatus.CONFLICT);
		return enrollmentRepository.save(new Enrollment(null, course, trainee, new Date(), null));
	}

	private Boolean checkIfEnrollmentExists(Course course, Trainee trainee) {
		return enrollmentRepository.existsByTraineeAndCourse(trainee, course);
	}

	public List<Course> getTrainerCourses(Long id, Pageable page) {
		return repository.findByTrainer(getTrainerObject(id), page);
	}

	public List<Course> getAll(String name, String category, Pageable page) {
		return repository.getAll(name, category, page);
	}

	public void deleteCourse(Long id, Long courseId) {
		Course course = getCourseByIdAndTrainer(id, courseId);
		Long enrollmentCount = enrollmentRepository.countByCourse(course);
		if (enrollmentCount > 0)
			throw new CustomException("course.delete.forbidden", HttpStatus.NOT_ACCEPTABLE);
		repository.deleteById(courseId);

	}

	private Trainer getTrainerObject(Long id) {
		return Trainer.builder().id(id).build();
	}

	public Course updateCourse(Long id, Long courseId, CourseDto dto) {
		VALIDATOR.validateOnUpdate(dto);
		Course course = getCourseByIdAndTrainer(id, courseId);
		Course updatedCourse = mapper.updateEntity(course, dto);
		if (checkIfExists(id, updatedCourse.getName(), updatedCourse.getCategory(), updatedCourse.getTrainer()))
			throw new CustomException("course.duplicated", HttpStatus.CONFLICT);
		return repository.save(updatedCourse);
	}

	private Course getCourseByIdAndTrainer(Long trainerId, Long courseId) {
		return repository.findByTrainerAndId(getTrainerObject(trainerId), courseId)
				.orElseThrow(() -> new CustomException("course.not_found", HttpStatus.NOT_FOUND));
	}

	public boolean checkIfTrainerHasCourses(Trainer trainer) {
		return repository.existsByTrainer(trainer);
	}
}

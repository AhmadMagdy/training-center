package com.magdy.trainingcenter.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.magdy.trainingcenter.dto.CourseDto;
import com.magdy.trainingcenter.dto.TrainerDto;
import com.magdy.trainingcenter.entity.Course;
import com.magdy.trainingcenter.entity.Trainer;
import com.magdy.trainingcenter.exception.CustomException;
import com.magdy.trainingcenter.mapper.TrainerMapper;
import com.magdy.trainingcenter.repository.TrainerRepository;
import com.magdy.trainingcenter.validator.TrainerValidator;

@Service
public class TrainerService {
	@Autowired
	private TrainerRepository repository;
	@Autowired
	private CourseService courseService;
	@Autowired
	private TrainerMapper mapper;
	private final static TrainerValidator VALIDATOR = new TrainerValidator();

	public Trainer add(TrainerDto dto) {
		VALIDATOR.validateOnInsert(dto);
		Trainer trainer = mapper.toEntity(dto);
		if (checkIfExists(null, trainer.getEmail()))
			throw new CustomException("trainer.duplicated", HttpStatus.NOT_ACCEPTABLE);
		return repository.save(trainer);
	}

	public Trainer update(Long id, TrainerDto dto) {
		VALIDATOR.validateOnUpdate(dto);
		Trainer trainer = getById(id);
		Trainer updatedTrainer = mapper.updateEntity(trainer, dto);
		if (checkIfExists(id, updatedTrainer.getEmail()))
			throw new CustomException("trainer.duplicated", HttpStatus.NOT_ACCEPTABLE);
		return repository.save(updatedTrainer);
	}

	public boolean checkIfExists(Long id, String email) {
		if (id != null) {
			Optional<Trainer> trainer = repository.findByIdNotAndEmailIgnoreCase(id, email);
			return trainer.isPresent();
		}
		return repository.findByEmailIgnoreCase(email).isPresent();
	}

	public Trainer getById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new CustomException("trainer.not_found", HttpStatus.NOT_FOUND));
	}

	public void delete(Long id) {
		Trainer trainer = getById(id);
		if (courseService.checkIfTrainerHasCourses(trainer))
			throw new CustomException("trainer.delete.forbidden", HttpStatus.NOT_ACCEPTABLE);
		else
			repository.delete(trainer);
	}

	public Course addCourse(Long id, CourseDto dto) {
		Trainer trainer = getById(id);
		dto.setTrainer(trainer);
		return courseService.add(dto);
	}

	public List<Course> getTrainerCourses(Long id, Pageable page) {
		return courseService.getTrainerCourses(id, page);
	}

	public void deleteCourse(Long id, Long courseId) {
		courseService.deleteCourse(id, courseId);
	}

	public Course updateCourse(Long id, Long courseId, CourseDto dto) {
		return courseService.updateCourse(id, courseId, dto);
	}

	public List<Trainer> getAll(Pageable page, String name, String email) {
		return repository.getAll(name, email, page);
	}
}

package com.magdy.trainingcenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magdy.trainingcenter.entity.Course;
import com.magdy.trainingcenter.entity.Enrollment;
import com.magdy.trainingcenter.entity.Trainee;

@Repository
public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {

	Long countByCourse(Course course);

	Boolean existsByTraineeAndCourse(Trainee tr, Course c);

}

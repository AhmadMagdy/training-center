package com.magdy.trainingcenter.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.magdy.trainingcenter.entity.Trainer;

public interface TrainerRepository extends JpaRepository<Trainer, Long> {

	@Query(value = "SELECT t FROM Trainer t WHERE t.name LIKE %?1% AND t.email LIKE %?2%")
	List<Trainer> getAll(String name, String email, Pageable page);

	@Query(value = "select count(t.id) FROM Trainer t WHERE t.name LIKE %?1% AND t.email LIKE %?2%")
	Long count(String name, String email);

	Optional<Trainer> findByIdNotAndEmailIgnoreCase(Long id, String email);

	Optional<Trainer> findByEmailIgnoreCase(String email);
}

package com.magdy.trainingcenter.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.magdy.trainingcenter.entity.Trainee;

@Repository
public interface TraineeRepository extends JpaRepository<Trainee, Long> {
	@Query("SELECT tr FROM Trainee tr WHERE tr.name LIKE %?1% AND tr.email LIKE %?2%")
	List<Trainee> filter(String name, String email, Pageable page);

	@Query(value = "SELECT DISTINCT(tr.id),tr.name,tr.email FROM trainees tr JOIN course_enrollment ce"
			+ " ON tr.id = ce.trainee_id"
			+ " WHERE tr.name LIKE %?1% AND tr.email LIKE %?2% AND ce.course_id = ?3", nativeQuery = true)
	List<Trainee> filter(String name, String email, Long courseId, Pageable page);

}

package com.magdy.trainingcenter.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.magdy.trainingcenter.entity.Course;
import com.magdy.trainingcenter.entity.Trainer;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
	List<Course> findByTrainer(Trainer trainer, Pageable page);

	@Query(value = "SELECT c FROM Course c WHERE c.name LIKE %?1% AND c.category LIKE %?2%")
	List<Course> getAll(String name, String category, Pageable page);

	Optional<Course> findByTrainerAndId(Trainer trainer, Long courseId);

	Boolean existsByTrainer(Trainer trainer);

	Optional<Course> findByIdNotAndNameIgnoreCaseAndCategoryIgnoreCaseAndTrainer(Long id, String name, String category,
			Trainer trainer);

	Optional<Course> findByNameIgnoreCaseAndCategoryIgnoreCaseAndTrainer(String name, String category, Trainer trainer);
}
